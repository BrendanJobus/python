package main

// Notes: to make changes to an array or slice that persist after a functions scope, use a pointer to that slice

import (
	"errors"
	"fmt"
	"strconv"
)

type Tree struct {
	Root *Node
	isBinary bool
}

type Node struct {
	Value string
	Right *Node
	Left  *Node
	Children []*Node
}

/*
func main() {		//
	// Tree to make
	// 		a
	//	  /	  \
	//	b	|	c
	//	 \	d  /
	//	  \	| /
	//		e
	t := DAG{}

	c := make([]rune, 0)
	t.insertDAG('a',c)

	c1 := make([]rune, 3)
	c1[0] = 'b'
	c1[1] = 'c'
	c1[2] = 'd'
	t.insertDAG('a', c1)
	d := make([]rune, 1)
	d[0] = 'd'
	t.insertDAG('b', d)
	t.insertDAG('c', d)
	e := make([]rune, 1)
	e[0] = 'e'
	t.insertDAG('c', e)
	t.insertDAG('a', e)
	t.insertDAG('d', e)
	fmt.Printf("%c\n", findLCADAG(t.Root, 'c', 'e'))
}
*/

// Creates a binary tree
func (t *Tree) insertBinary(val int) error {
	if t.Root == nil {
		t.isBinary = true
		t.Root = &Node{Value: fmt.Sprint(val)}

		
		return nil
	} else if t.isBinary == false {
		return errors.New("Tree is not binary")
	} else {
		parent, err := findFit(t.Root, val)
		if err != nil {
			return err
		}
		pVal := parent.Value
		pInt, err := strconv.Atoi(pVal)
		if err != nil {
			return errors.New("Conversion Error")
		}

		node := &Node{Value: fmt.Sprint(val)}
		if pInt < val {
			parent.Right = node
		} else {
			parent.Left = node
		}
		
		parent.Children = append(parent.Children, node)
		return nil
	}
}

// finds the position that a int will fit into in a binary tree
func findFit(node *Node, input int) (*Node, error) {
	current, err := strconv.Atoi(node.Value)
	if err != nil {
		return node, errors.New("Conversion Error")
	} else {
		if current == input {
			return node, errors.New("Integer already inputed into tree")
		} else {
			if (current > input && node.Left == nil) || (current < input && node.Right == nil) {
				return node, nil
			} else if current > input && node.Left != nil {
				return findFit(node.Left, input)
			} else {
				return findFit(node.Right, input)
			}
		}
	}
}

// Will return an error if there is no such parent rune already in the tree and the tree is non-empty
func (t *Tree) insert(parent string, newChildren []string) error {
	if t.Root == nil {	// if there are t is empty
		t.Root = &Node{Value: parent}	// make parent root
		t.isBinary = false
		var e error
		e = nil
		if len(newChildren) > 0 {			// if newChildren is not empty
			e = t.Root.insert(*t, parent, newChildren)	// call insert
		}
		return e
	} else if t.isBinary == true {
		return errors.New("This is a binary tree")
	} else {
		return t.Root.insert(*t, parent, newChildren)
	}
}

func (n *Node) insert(t Tree, parent string, newChildren []string) error {
	nodeParent, found := n.findNode(parent)
	if found == true {
		for i := 0; i < len(newChildren); i++ {
			node, look := t.Root.findNode(newChildren[i])
			if look == true {
				nodeParent.Children = append(nodeParent.Children, node)
			} else {
				newNode := &Node{Value: newChildren[i]}
				nodeParent.Children = append(nodeParent.Children, newNode)
			}
		}
	
		return nil
	} else { 
		return errors.New("No valid parent Node given") 
	}
}

func (n *Node) findNode(s string) (*Node, bool) {
	if n == nil {
		return nil, false
	}

	if s == n.Value {
		return n, true
	} else {
		var answer *Node
		found := false
		for i := 0; i < len(n.Children); i++ {
			answer, found = n.Children[i].findNode(s)
			if found == true { break }
		}

		return answer, found
	}
}

func findLCA(root *Node, c1, c2 string) string {
	var path1, path2 []string

	if !findPath(root, &path1, c1) || !findPath(root,&path2, c2) {
		return ""
	}

	var i int
	for i = 0; i < len(path1) && i < len(path2); i++ {
		if path1[i] != path2[i] {
			break
		}
	}
	if i > 0 {
		return path1[i - 1]
	}
	return path1[i]
}

func findPath(root *Node, path *[]string, k string) bool {
	if root == nil { return false }
	*path = append(*path, root.Value)

	if root.Value == k { return true }

	for i := 0; i < len(root.Children); i++ {
		if findPath(root.Children[i], path, k) { return true }
	}
	
	*path = (*path)[:len(*path) - 1]
	return false
}