package main

import "testing"

func TestInsertBinary(t *testing.T) {
  values := []int{10, 5, 13, 6, 4, 15, 2, 7, 35}					//				  10
                                                          //			  5	  13
	tree := &Tree{}													                //		  4  6	 15
	for i := 0; i < len(values); i++ {								      //		 2    7 	 35
		err := tree.insertBinary(values[i])
		if err != nil {
			t.Errorf("Error inserting value %v", values[i])
		} else {
      t.Logf("Value inserted '%v'", values[i])
    }
  }
  err := tree.insertBinary(10)
  if err != nil {
    t.Logf("Repeat values caught and not inputed")
  } else {
    t.Errorf("Repeat values not caught")
  }
}

func TestFirstChildren(t *testing.T) {
  values := []int{10, 5, 13}

  tree := &Tree{}
  for i := 0; i < len(values); i++ {
    tree.insertBinary(values[i])
  }

  result := findLCA(tree.Root, "5", "13")
  if result != "10" {
    t.Errorf("findLCA(tree.Root, 5, 13) failed, expected %v, got %v", 10, result)
  } else {
    t.Logf("findLCA(tree.Root, 5, 13) success, expected %v, got %v", 10, result)
  }
}

func TestSecondChildren(t *testing.T) {
  values := []int{10, 5, 13, 6, 4, 15}

  tree := &Tree{}
  for i := 0; i < len(values); i++ {
    tree.insertBinary(values[i])
  }

  result := findLCA(tree.Root, "6", "4")
  if result != "5" {
    t.Errorf("findLCA(tree.Root, 6, 4) failed, expected %v, got %v", 5, result)
  } else {
    t.Logf("findLCA(tree.Root, 6, 4) success, expected %v, got %v", 5, result)
  }

  result = findLCA(tree.Root, "6", "15")
  if result != "10" {
    t.Errorf("findLCA(tree.Root, 6, 15) failed, expected %v, got %v", 10, result)
  } else {
    t.Logf("findLCA(tree.Root, 6, 15) success, expected %v, got %v", 10, result)
  }

  result = findLCA(tree.Root, "4", "15")
  if result != "10" {
    t.Errorf("findLCA(tree.Root, 4, 15) failed, expected %v, got %v", 10, result)
  } else {
    t.Logf("findLCA(tree.Root, 4, 15) success, expected %v, got %v", 10, result)
  }
}

func TestBetweenRows(t *testing.T) {
  values := []int{10, 5, 13, 6, 4, 15, 2, 7, 35}

  tree := &Tree{}
  for i := 0; i < len(values); i++ {
    tree.insertBinary(values[i])
  }

  result := findLCA(tree.Root, "2", "6")
  if result != "5" {
    t.Errorf("findLCA(tree.Root, 4, 6) failed, expected %v, got %v", 5, result)
  } else {
    t.Logf("findLCA(tree.Root, 2, 6) success, expected %v, got %v", 5, result)
  }

  result = findLCA(tree.Root, "2", "15")
  if result != "10" {
    t.Errorf("findLCA(tree.Root, 2, 15) failed, expected %v, got %v", 10, result)
  } else {
    t.Logf("findLCA(tree.Root, 2, 15) success, expected %v, got %v", 10, result)
  }
}

func TestCallOnNonExistentNode(t *testing.T) {
  values := []int{10, 5, 13, 6, 4, 15, 2, 7, 35}

  tree := &Tree{}
  for i := 0; i < len(values); i++ {
    tree.insertBinary(values[i])
  }

  result := findLCA(tree.Root, "2", "9")
  if result != "" {
    t.Errorf("findLCA(2, 9) on non-existent 9 failed, expected %v, got %v", -1, result)
  } else {
    t.Logf("findLCA(2, 9) on non-existent 9 success, expected %v, got %v", -1, result)
  }

  result = findLCA(tree.Root, "1", "6")
  if result != "" {
    t.Errorf("findLCA(1, 6) on non-existent 1 failed, expected %v, got %v", -1, result)
  } else {
    t.Logf("findLCA(1, 6) on non-existent 1 success, expected %v, got %v", -1, result)
  }

  result = findLCA(tree.Root, "6", "39")
  if result != "" {
    t.Errorf("findLCA(6, 39) on non-existent 39 failed, expected %v, got %v", -1, result)
  } else {
    t.Logf("findLCA(6, 39) on non-existent 39 success, expected %v, got %v", -1, result)
  }

  result = findLCA(tree.Root, "9", "39")
  if result != "" {
    t.Errorf("findLCA(9, 39) on non-existent 9 and non-existent 39 failed, expected %v, got %v", -1, result)
  } else {
    t.Logf("findLCA(9, 39) on non-existent 9 and non-existent 39 success, expected %v, got %v", -1, result)
  }
}

func TestEmptyDAGTree(t *testing.T) {
  tree := &Tree{}

  emptyTree := findLCA(tree.Root, "a", "b")
  if emptyTree != "" {
    t.Errorf("LCA failed, function does not work for empty tree")
  } else {
     t.Logf("LCA passed")
   }
}

func TestEmptyInsertDAG(t *testing.T) {
  tree := &Tree{}

  a := make([]string, 0)
  err := tree.insert("a", a)

  if err != nil {
    t.Errorf("Insert Failed")
  } else {
    t.Logf("Insert Passed")
  }

  tree = &Tree{}
  b := make([]string, 3)
  b[0] = "b"
  b[1] = "c"
  b[2] = "d"
  err = tree.insert("a", b)

  if err != nil {
    t.Errorf("Insert Failed")
  } else {
    t.Logf("Insert Passed")
  }
}

func TestNonEmptyInsertDAG(t *testing.T) {
  tree := &Tree{}

  a := make([]string, 0)
  tree.insert("a", a)

  b := make([]string, 3)
  b[0] = "b"
  b[1] = "c"
  b[2] = "d"
  err := tree.insert("a", b)

  if err != nil && tree.Root.Children[0].Value != "b" && tree.Root.Children[1].Value != "c" && tree.Root.Children[2].Value != "d" {
    t.Errorf("Insert failed")
  } else {
     t.Logf("Insert passed")
   }
}

func TestMultipleParentsInsertDAG(t *testing.T) {
  tree := &Tree{}

  a := make([]string, 0)
  tree.insert("a", a)

  b := make([]string, 3)
  b[0] = "b"
  b[1] = "c"
  b[2] = "d"
  tree.insert("a", b)

  c := make([]string, 1)
  c[0] = "c"
  err := tree.insert("b", c)

  if err != nil && tree.Root.Children[1].Value != "c" && tree.Root.Children[0].Children[0].Value != "c" {
    t.Errorf("Insert Failed")
  } else {
    t.Logf("Insert Passed")
  }
}

func TestInvalidInsertDAG(t *testing.T) {
  tree := &Tree{}

  a := make([]string, 0)
  tree.insert("a", a)
  b := make([]string, 1)
  b[0] = "z"
  err := tree.insert("b", b)
  
  if err == nil {
    t.Errorf("Insert Passed incorectly")
  } else {
    t.Logf("Insert failed correctly")
  }
}

func TestRegularLCADAG(t *testing.T) {
  tree := &Tree{} 
  
  e := make([]string, 0)
  tree.insert("a", e)

  a := make([]string, 3)
  a[0] = "b"
  a[1] = "c"
  a[2] = "d"
  tree.insert("a", a)

  x := findLCA(tree.Root, "a", "a")

  if x != "a" {
    t.Errorf("Answer incorrect")
  } else {
    t.Logf("Answer correct")
  }

  x = findLCA(tree.Root, "a", "c")

  if x != "a" {
    t.Errorf("Answer incorrect")
  } else {
    t.Logf("Answer correct")
  }

  x = findLCA(tree.Root, "b", "c")

  if x != "a" {
    t.Errorf("Answer incorrect")
  } else {
    t.Logf("Answer correct")
  }

  b := make([]string, 2)
  b[0] = "e"
  b[1] = "f"
  tree.insert("b", b)

  x = findLCA(tree.Root, "f", "c")

  if x != "a" {
    t.Errorf("Answer incorrect")
  } else {
    t.Logf("Answer correct")
  }

  x = findLCA(tree.Root, "f", "b")

  if x != "b" {
    t.Errorf("Answer incorrect")
  } else {
    t.Logf("Answer correct")
  }
}

func TestCrossTypeInsert(t *testing.T) {
  tree := &Tree{}

  tree.insertBinary(10)
  a := make([]string, 1)
  a[0] = "c"
  tree.insert("a", a)

  tr := &Tree{}

  tr.insert("a", a)
  tr.insertBinary(10)
}